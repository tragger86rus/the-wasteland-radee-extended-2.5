Key,Source,Context,Changes,English,Russian

.30-06 Cal,items,Ammo,New,"Патрон 30-06"
.30-06 Cal/Steel Core,items,Ammo,New,"Бронебойный Патрон 30-06"

30-06 Lever Rifle,items,Gun,New,"Крупнокалиберная рычажная винтовка"
30-06 Lever RifleDesc,items,Gun,New,"Патрон 30-06"

M1 Garand,items,Gun,New,"М1 Гаранд"
M1 GarandDesc,items,Gun,New,"Патрон 30-06"

Browning Automatic Rifle,items,Gun,New,"Легкий пулемет М19"
Browning Automatic RifleDesc,items,Gun,New,"Патрон 30-06"

Browning Heavy Machinegun,items,Gun,New,"Тяжелый пулемет с охлаждением"
Browning Heavy MachinegunDesc,items,Gun,New,"Патрон 30-06"